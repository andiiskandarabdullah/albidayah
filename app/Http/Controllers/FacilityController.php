<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function index()
    {
        return view('admin.facility.index', [
            'facilities' => Facility::orderBy('id', 'desc')->get()
        ]);
    }

    public function store(Request $request)
    {
        $rules = [
            'judul' => 'required',
            'image' => 'required|max:1000|mimes:jpg,jpeg,png,webp',
        ];

        $messages = [
            'judul.required' => 'Judul wajib diisi!',
            'image.required' => 'Image wajib diisi!',
        ];

        $this->validate($request, $rules, $messages);

        // Image
        $fileName = time() . '.' . $request->image->extension();
        $request->file('image')->storeAs('public/facility', $fileName);

        Facility::create([
            'judul' => $request->judul,
            'image' => $fileName,
        ]);
        return redirect(route('facility'))->with('success', 'data kegiatan berhasil di simpan');
    }

    public function update(Request $request, $id)
    {
        $facility = Facility::find($id);

        # Jika ada image baru
        if ($request->hasFile('image')) {
            $fileCheck = 'required|max:1000|mimes:jpg,jpeg,png';
        } else {
            $fileCheck = '';
        }

        $rules = [
            'judul' => 'required',
            'image' => $fileCheck,
        ];

        $messages = [
            'judul.required' => 'Judul wajib diisi!',
            'image.required' => 'Image wajib diisi!',
        ];

        $this->validate($request, $rules, $messages);

        // Cek jika ada image baru
        if ($request->hasFile('image')) {
            if (\File::exists('storage/facility/' . $facility->image)) {
                \File::delete('storage/facility/' . $request->old_image);
            }
            $fileName = time() . '.' . $request->image->extension();
            $request->file('image')->storeAs('public/facility', $fileName);
        }

        if ($request->hasFile('image')) {
            $checkFileName = $fileName;
        } else {
            $checkFileName = $request->old_image;
        }
        $facility->update([
            'judul' => $request->judul,
            'image' => $checkFileName,
        ]);

        return redirect(route('facility'))->with('success', 'data photo berhasil di update');
    }

    public function destroy($id)
    {
        $facility = Facility::find($id);
        if (\File::exists('storage/facility/' . $facility->image)) {
            \File::delete('storage/facility/' . $facility->image);
        }

        $facility->delete();

        return redirect(route('facility'))->with('success', 'data berhasil di hapus');
    }
}
