<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Facility;
use App\Models\Photo;
use App\Models\Video;
use App\Models\ProfilPimpinan;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        return view('welcome', [
            'artikels' => Blog::orderBy('id', 'desc')->limit(3)->get(),
            'videos' => Video::orderBy('id', 'desc')->limit(3)->get(),
            'facilities' => Facility::orderBy('id', 'desc')->limit(4)->get(),
            'photos' => Photo::orderBy('id', 'desc')->limit(4)->get(),
        ]);
    }
    public function berita()
    {
        return view('berita.berita', [
            'artikels' => Blog::orderBy('id', 'desc')->get()
        ]);
    }
    public function listVideo()
    {
        return view('video.index', [
            'video' => Video::orderBy('id', 'desc')->get()
        ]);
    }
    public function listFoto()
    {
        return view('foto.foto', [
            'photos' => Photo::orderBy('id', 'desc')->get()
        ]);
    }
    public function listFasilitas()
    {
        return view('fasilitas.fasilitas', [
            'facilities' => Facility::orderBy('id', 'desc')->get()
        ]);
    }
    public function detail($slug)
    {
        $artikel = Blog::where('slug', $slug)->first();
        return view('berita.detail', [
            'artikel' => $artikel
        ]);
    }
    public function pimpinan()
    {
        $profil_pimpinan = ProfilPimpinan::orderBy('id', 'desc')->get();
        return view('pimpinan.pimpinan', [
            'pimpinan' => $profil_pimpinan
        ]);
    }
}
