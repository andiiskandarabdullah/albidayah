<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProfilPimpinan;

class ProfilPimpinanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $profil_pimpinan = ProfilPimpinan::orderBy('id', 'desc')->get();
        return view('admin.pimpinan.index', [
            'pimpinan' => $profil_pimpinan
        ]);
    }

    #Halaman Create
    public function create()
    {
        return view('admin.pimpinan.create');
    }

    #Fungsi Store
    public function store(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'jabatan' => 'required',
            'foto' => 'required|max:2000|mimes:jpg,jpeg,png,webp',
            'keterangan' => 'required|min:20',
        ];

        $messages = [
            'nama.required' => 'Nama wajib diisi!',
            'jabatan.required' => 'Jabatan wajib diisi!',
            'foto.required' => 'foto wajib diisi!',
            'keterangan.required' => 'Keterangan wajib diisi!',
        ];

        $this->validate($request, $rules, $messages);

        // Image
        $fileName = time() . '.' . $request->foto->extension();
        $request->file('foto')->storeAs('public/foto-pimpinan', $fileName);

        # Artikel
        $storage = "storage/foto-pimpinan";
        $dom = new \DOMDocument();

        # untuk menonaktifkan kesalahan libxml standar dan memungkinkan penanganan kesalahan pengguna.
        libxml_use_internal_errors(true);
        $dom->loadHTML($request->keterangan, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NOIMPLIED);
        # Menghapus buffer kesalahan libxml
        libxml_clear_errors();
        # Baca di https://dosenit.com/php/fungsi-libxml-php
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $img) {
            $src = $img->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                $fileNameContent = uniqid();
                $fileNameContentRand = substr(md5($fileNameContent), 6, 6) . '_' . time();
                $filePath = ("$storage/$fileNameContentRand.$mimetype");
                $image = Image::make($src)->resize(1440, 720)->encode($mimetype, 100)->save(public_path($filePath));
                $new_src = asset($filePath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
                $img->setAttribute('class', 'img-responsive');
            }
        }
        ProfilPimpinan::create([
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'foto' => $fileName,
            'keterangan' => $dom->saveHTML(),
        ]);
        return redirect(route('profil_pimpinan'))->with('success', 'data berhasil di simpan');
    }

    #Fungsi Edit
    public function edit($id)
    {
        $pimpinan = ProfilPimpinan::find($id);
        return view('admin.pimpinan.edit', [
            'pimpinan' => $pimpinan
        ]);
    }

    #Fungsi Update
    public function update(Request $request, $id)
    {
        $pimpinan = ProfilPimpinan::find($id);

        # Jika ada image baru
        if ($request->hasFile('foto')) {
            $fileCheck = 'required|max:2000|mimes:jpg,jpeg,png';
        } else {
            $fileCheck = '';
        }

        $rules = [
            'nama' => 'required',
            'jabatan' => 'required',
            'foto' => $fileCheck,
            'keterangan' => 'required|min:20',
        ];

        $messages = [
            'nama.required' => 'Nama wajib diisi!',
            'jabatan.required' => 'Jabatan wajib diisi!',
            'foto.required' => 'Foto wajib diisi!',
            'keterangan.required' => 'Keterangan wajib diisi!',
        ];

        $this->validate($request, $rules, $messages);

        // Cek jika ada image baru
        if ($request->hasFile('foto')) {
            if (\File::exists('storage/foto-pimpinan/' . $pimpinan->foto)) {
                \File::delete('storage/foto-pimpinan/' . $request->old_foto);
            }
            $fileName = time() . '.' . $request->foto->extension();
            $request->file('foto')->storeAs('public/foto-pimpinan', $fileName);
        }

        if ($request->hasFile('foto')) {
            $checkFileName = $fileName;
        } else {
            $checkFileName = $request->old_foto;
        }

        // Artikel
        $storage = "storage/foto-pimpinan";
        $dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($request->keterangan, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NOIMPLIED);
        libxml_clear_errors();

        $images = $dom->getElementsByTagName('img');

        foreach ($images as $img) {
            $src = $img->getAttribute('src');
            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                $fileNameContent = uniqid();
                $fileNameContentRand = substr(md5($fileNameContent), 6, 6) . '_' . time();
                $filePath = ("$storage/$fileNameContentRand.$mimetype");
                $image = Image::make($src)->resize(1200, 1200)->encode($mimetype, 100)->save(public_path($filePath));
                $new_src = asset($filePath);
                $img->removeAttribute('src');
                $img->setAttribute('src', $new_src);
                $img->setAttribute('class', 'img-responsive');
            }
        }

        $pimpinan->update([
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'foto' => $checkFileName,
            'keterangan' => $dom->saveHTML(),
        ]);

        return redirect(route('profil_pimpinan'))->with('success', 'data berhasil di update');
    }

    #Fungsi Delete
    public function destroy($id)
    {
        $pimpinan = ProfilPimpinan::find($id);
        if (\File::exists('storage/foto-pimpinan/' . $pimpinan->foto)) {
            \File::delete('storage/foto-pimpinan/' . $pimpinan->foto);
        }

        $pimpinan->delete();

        return redirect(route('profil_pimpinan'))->with('success', 'data berhasil di hapus');
    }
}
