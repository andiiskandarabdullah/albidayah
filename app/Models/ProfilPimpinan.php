<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilPimpinan extends Model
{
    use HasFactory;
    protected $table = 'profil_pimpinan';
    protected $fillable = [
        'nama',
        'jabatan',
        'keterangan',
        'riwayat_pendidikan',
        'pengalaman_organisasi',
        'kunjungan_luar_negri',
        'karya_tulis',
        'foto'
    ];
}
