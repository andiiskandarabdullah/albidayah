<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FacilitiesController;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\ProfilPimpinanController;
use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AppController::class, 'index']);

Route::get('/berita', [AppController::class, 'berita']);


Route::get('/detail/{slug}', [AppController::class, 'detail']);
Route::get('/list-foto', [AppController::class, 'listFoto'])->name('list-foto');
Route::get('/pimpinan', [AppController::class, 'pimpinan'])->name('pimpinan');
Route::get('/list-video', [AppController::class, 'listVideo'])->name('list-video');

Route::get('/list-fasilitas', [AppController::class, 'listFasilitas'])->name('list-fasilitas');

Route::get('/login', [AuthController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [AuthController::class, 'authenticate']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('auth');

Route::get('/blog', [BlogController::class, 'index'])->name('blog')->middleware('auth');
Route::get('/blog/create', [BlogController::class, 'create'])->name('blog.create')->middleware('auth');
Route::post('/blog/store', [BlogController::class, 'store'])->name('blog.store')->middleware('auth');
Route::get('/blog/edit/{id}', [BlogController::class, 'edit'])->name('blog.edit')->middleware('auth');
Route::post('/blog/update/{id}', [BlogController::class, 'update'])->name('blog.update')->middleware('auth');
Route::post('/blog/destroy/{id}', [BlogController::class, 'destroy'])->name('blog.destroy')->middleware('auth');

Route::get('/photo', [PhotoController::class, 'index'])->name('photo')->middleware('auth');
Route::post('/photo/store', [PhotoController::class, 'store'])->name('photo.store')->middleware('auth');
Route::post('/photo/update/{id}', [PhotoController::class, 'update'])->name('photo.update')->middleware('auth');
Route::post('/photo/destroy/{id}', [PhotoController::class, 'destroy'])->name('photo.destroy')->middleware('auth');

Route::get('/video', [VideoController::class, 'index'])->name('video')->middleware('auth');
Route::post('/video/store', [VideoController::class, 'store'])->name('video.store')->middleware('auth');
Route::post('/video/update/{id}', [VideoController::class, 'update'])->name('video.update')->middleware('auth');
Route::post('/video/destroy/{id}', [VideoController::class, 'destroy'])->name('video.destroy')->middleware('auth');

Route::get('/facility', [FacilityController::class, 'index'])->name('facility')->middleware('auth');
Route::post('/facility/store', [FacilityController::class, 'store'])->name('facility.store')->middleware('auth'); //
Route::post('/facility/update/{id}', [FacilityController::class, 'update'])->name('facility.update')->middleware('auth');
Route::post('/facility/destroy/{id}', [FacilityController::class, 'destroy'])->name('facility.destroy')->middleware('auth');

Route::get('/profil_pimpinan', [ProfilPimpinanController::class, 'index'])->name('profil_pimpinan')->middleware('auth');
Route::get('/profil_pimpinan/create', [ProfilPimpinanController::class, 'create'])->name('profil_pimpinan.create')->middleware('auth');
Route::post('/profil_pimpinan/store', [ProfilPimpinanController::class, 'store'])->name('profil_pimpinan.store')->middleware('auth');
Route::get('/profil_pimpinan/edit/{id}', [ProfilPimpinanController::class, 'edit'])->name('profil_pimpinan.edit')->middleware('auth');
Route::post('/profil_pimpinan/update/{id}', [ProfilPimpinanController::class, 'update'])->name('profil_pimpinan.update')->middleware('auth');
Route::post('/profil_pimpinan/destroy/{id}', [ProfilPimpinanController::class, 'destroy'])->name('profil_pimpinan.destroy')->middleware('auth');
