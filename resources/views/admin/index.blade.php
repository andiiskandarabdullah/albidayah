@extends('layouts.layouts')

@section('content')
    <section style="margin-top: 100px">
        <div class="container col-xxl-8 py-5">
            <h3 class="fw-bold mb-2">Halaman Dashboard Admin</h3>
            <p>Selamat Datang di halaman dashboard admin</p>

            <div class="row">
                <div class="col-lg-4">
                    <div class="card shadow-sm rounded-3 border-0">
                        <img src="{{ asset('assets/images/il-bg-al.webp') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Blog Artikel</h5>
                            <p class="card-text">Atur dan Kelola artikel kegiatan pesantren</p>
                            <a href="{{ route('blog') }}" class="btn btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card shadow-sm rounded-3 border-0">
                        <img src="{{ asset('assets/images/il-photo-01.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Photo Kegiatan</h5>
                            <p class="card-text">Atur dan Kelola photo kegiatan pesantren</p>
                            <a href="{{ route('photo') }}" class="btn btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card shadow-sm rounded-3 border-0">
                        <img src="{{ asset('assets/images/il-photo-01.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Video Kegiatan</h5>
                            <p class="card-text">Atur dan Kelola video kegiatan pesantren</p>
                            <a href="{{ route('video') }}" class="btn btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card shadow-sm rounded-3 border-0">
                        <img src="{{ asset('assets/images/il-photo-01.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Fasilitas Pesantren</h5>
                            <p class="card-text">Atur dan Kelola fasilitas pesantren</p>
                            <a href="{{ route('facility') }}" class="btn btn-primary">Detail</a>
                        </div>
                    </div>
                </div>
								<div class="col-lg-4">
									<div class="card shadow-sm rounded-3 border-0">
											<img src="{{ asset('assets/images/il-photo-01.png') }}" class="card-img-top" alt="...">
											<div class="card-body">
													<h5 class="card-title">Profil Pimpinan</h5>
													<p class="card-text">Atur dan Kelola Profil Pimpinan</p>
													<a href="{{ route('profil_pimpinan') }}" class="btn btn-primary">Detail</a>
											</div>
									</div>
							</div>
            </div>
        </div>
    </section>
@endsection
