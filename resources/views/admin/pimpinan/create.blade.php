@extends('layouts.layouts')

@section('content')
    <section class="py-5" style="margin-top: 100px">
        <div class="container col-xxl-8 py-5">

            {{-- Navigasi --}}
            <div class="d-flex">
                <a href="{{ route('profil_pimpinan') }}">Profil Pimpinan</a>
                <div class="mx-1">.</div>
                <a href="">Buat Profil Pimpinan</a>
            </div>
            <h4>Halaman Buat Profil Pimpinan</h4>
            <form action="{{ route('profil_pimpinan.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group mb-4">
                    <label for="">Masukan Nama Pimpinan</label>
                    <input type="text" class="form-control @error('nama')
                        is-invalid @enderror"
                        name="nama" value="{{ old('nama') }}">

                    @error('nama')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="">Masukan Jabatan</label>
                    <input type="text" class="form-control @error('jabatan')
                        is-invalid @enderror"
                        name="jabatan" value="{{ old('jabatan') }}">

                    @error('jabatan')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="">Pilih Photo Profil</label>
                    <input type="file" class="form-control @error('foto') is-invalid @enderror" name="foto"
                        value="{{ old('foto') }}">

                    @error('foto')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="summernote">
                        {{ old('keterangan') }}
                    </textarea>

                    @error('keterangan')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </section>
@endsection
