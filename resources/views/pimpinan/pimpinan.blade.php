

{{-- @php
        dd($pimpinan);
@endphp --}}

@extends('layouts.layouts')
<style>
#rcorners2 {
  border-radius: 25px;
  border: 2px solid #73AD21;
  padding: 20px;
  margin-top: 10px;
}
</style>
@section('content')
    {{-- profil_pimpinan --}}
    <section id="profil_pimpinan" style="margin-top: 100px" class="py-5">
        @foreach ($pimpinan as $item)
            <div class="container" id="rcorners2">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="mb-2">
                            <img class="w-70" src="{{ asset('storage/photo/' . $item->foto) }}" alt="">
                        </div>
                        <div class="mb-2 d-flex">
                            <h4 class="font-weight-normal">{{$item->nama}}</h4>
                        </div>
                        <div class="mb-2">
                            <ul class="list-unstyled">
                                <li class="media">
                                <label class="media-body">{{$item->jabatan}}</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6 pl-xl-3">
                        {!! $item->keterangan !!}
                    </div>
                </div>
            </div>
        @endforeach
    </section>
    {{-- profil_pimpinan --}}
@endsection
