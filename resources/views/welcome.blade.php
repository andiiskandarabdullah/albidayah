@extends('layouts.layouts')

@section('content')
    <section id="hero" class="px-0">
        <div class="container text-center text-white">
            <div class="hero-tittle" data-aos="fade-up">
                <div>
                    <h1><strong>Selamat Datang Di Pesantren<br>Terpadu Al-Bidayah Cangkorah</strong></h1>
                    <h4>Pondok Pesantren dengan Konsep Ahlusunnah Wal Jama'ah</h4>
                </div>
            </div>
        </div>
    </section>

    <section id="program" style="margin-top:-30px">
        <div class="container col-xxl-9 ">
            <div class="row">
                <div class="col-lg-4 col-md-5 col mb-3 " data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p>Madrasah Aliyah Swasta <br> Al-Bidayah Cangkorah</p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-book.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col mb-3" data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p>Madrasah Tsanawiyah Terpadu <br> Al-Bidayah Cangkorah </p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-globe.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col mb-3" data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p> Roudhatul Athfal <br> Al-Bidayah Cangkorah</p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-neraca.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col mb-3" data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p>Kelompok Bimbingan Ibadah Haji <br> Al-Bidayah Cangkorah</p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-komputer.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col mb-3" data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p>Pondok Pesantren Terpadu <br> Al-Bidayah Cangkorah</p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-komputer.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col mb-3" data-aos="flip-left">
                    <div class="bg-white rounded-3 shadow p-3 d-flex justify-content-between align-items-center">
                        <div>
                            <p>Himpunan Alumni Santri <br> Al-Bidayah Cangkorah</p>
                        </div>
                        <img src="{{ asset('assets/icons/ic-komputer.png') }}" height="55" width="55" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- berita --}}
    <section id="berita" class="py-5">
        <div class="container">
            <div class="header-berita text-center">
                <h2 class="fw-bold">Berita Kegiatan Pondok Pesantren</h2>
            </div>
            <div class="row py-5" data-aos="flip-up">
                @foreach ($artikels as $item)
                    <div class="col-lg-4">
                        <div class="card border-0">
                            <img src="{{ asset('storage/artikel/' . $item->image) }}" class="image-fluid mb-3    "
                                alt="">
                            <div class="konten-berita">
                                <p class="mb-3" text-secondary>{{ $item->create_at }}</p>
                                <h4 class="fw-bold mb-3">{{ $item->judul }}</h4>
                                <p class="text-secondary">#albidayahcangkorah</p>
                                <a href="/detail/{{ $item->slug }}"
                                    class="text-decoration-none text-denger">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="footer-berita text-center">
                <a href="/berita" class="btn btn-outline-danger">Berita Lainya</a>
            </div>
        </div>
    </section>
    {{-- berita --}}
    {{-- Join --}}
    <section id="join" class="py-5" data-aos="flip-down">
        <div class="container py-5">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6">
                    <div class="d-flex align-items-center mb-3">
                        <div class="stripe me-2"></div>
                        <h5>Daftar Santri</h5>
                    </div>
                    <h1 class="fw-bold mb-2">Gabung bersama kami, mewujudkan generasi emas yang menjunjung tinggi
                        Aqidah
                        Ahlusunnah Wal
                        Jama'ah
                    </h1>
                    <p class="mb-3">
                        Pondok Pesantren ialah salah satu wujud pembelajaran yang tertua di Indonesia serta butuh
                        dipelihara kelestariannya dalam bagan usaha mencerdaskan bangsa. Pesantren Albidayah Cangkorah
                        Kecamatan Batujajar Kabupaten Bandung Barat merupakan salah satu Pondok Pesantren yang mempunyai
                        guna itu.

                        Pondok Pesantren Albidayah dibuat oleh K. H. Muhammad Asy’ arie pada tahun 1907 meski pada
                        dikala itu sedang dalam kondisi yang amat simpel. Dimulai dengan suatu mesjid yang sekalian jadi
                        tempat berlatih serta menginap santri- santrinya yang berdatangan dari kampung- kampung di dekat
                        pesantren, ditambah dengan rumah tempat adres kiyai.
                    </p>
                    <button class="btn btn-outline-danger">Register</button>
                </div>
                <div class="col-lg-6">
                    <img src="{{ asset('assets/images/il-berita-03.jpg') }}" class="img-fluid" alt="">
                </div>
            </div>
        </div>

    </section>
    {{-- Join --}}

    {{-- Video --}}
    <section id="video" class="py-5" data-aos="zoom-in">
        <div class="container py-5">
            <div class="text-center">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/mhID1AWi0-M?si=IAYq3C9e13Kz7Ln4"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowfullscreen>
                </iframe>
            </div>
        </div>

    </section>
    {{-- Video --}}
    <section id="video_youtobe" class="py-5" data-aos="zoom-in">
        <div class="container py-5">
            <div class="header-berita text-center">
                <h2 class="fw-bold">Video Kegiatan Pondok Pesantren</h2>
            </div>
            <div class="row py-5" data-aos="zoom-in">
                @foreach ($videos as $item)
                    <div class="col-lg-4">
                        <iframe width="100%" height="215"
                            src="https://www.youtube.com/embed/{{ $item->youtube_code }}" title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                            allowfullscreen></iframe>
                    </div>
                @endforeach
            </div>
            <div class="footer-berita text-center">
                <a href="/list-video" class="btn btn-outline-danger">Video Lainnya</a>
            </div>
        </div>

    </section>
    {{-- Foto --}}
    <section id="foto" class="section-foto parallax" data-aos="zoom-in-up">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center mb-5">
                <div class="d-flex align-items-center">
                    <div class="stripe-putih me-2"></div>
                    <h5 class="fw-bold text-white">Foto Kegiatan</h5>
                </div>
                <div>
                    <a href="/list-foto" class="btn btn-outline-white">Foto Lainnya</a>
                </div>
            </div>
            <div class="row">
                @foreach ($photos as $photo)
                    <div class="col-lg-3 col-md-6 col-6">
                        <a class="image-link" href="{{ asset('storage/photo/' . $photo->image) }}">
                            <img src="{{ asset('storage/photo/' . $photo->image) }}" class="img-fluid" alt="">
                        </a>
                        <p>{{ $photo->judul }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    {{-- Fasilitas --}}
    <section id="fasilitas" class="py-5" data-aos="zoom-in-up">
        <div class="container">
            <div class="container py-5">
                <div class="text-center">
                    <h3 class="fw-bold">Fasilitas Pesantren</h3>
                </div>
                <div class="d-flex justify-content-between align-items-center mb-5">
                </div>
                <div class="row">
                    @foreach ($facilities as $facility)
                        <div class="col-lg-3 col-md-6 col-6">
                            <a class="image-link" href="{{ asset('storage/facility/' . $facility->image) }}">
                                <img src="{{ asset('storage/facility/' . $facility->image) }}" class="img-fluid"
                                    alt="">
                            </a>
                            <p>{{ $facility->judul }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="text-center">
                <a href="/list-fasilitas" class="btn btn-outline-danger">Fasilitas Lainya</a>
            </div>
        </div>
    </section>
@endsection


{{-- Footer --}}

{{-- Footer --}}
