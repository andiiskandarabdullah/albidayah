<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ asset('assets/icons/ic-logo.ico') }}">
    {{-- Meta untuk tampil di Whatsapp --}}
    @if (Request::segment(1) == '')
        <meta property="og:title" content="Pesantren Al-Bidayah Cangkorah" />
        <meta name="description" content="Pesantren Moderan dengan Fasilitas Lengkap" />
        <meta property="og:url" content="http://albidayahcangkorah.sch.id" />
        <meta property="og:description" content="Pesantren Al-Bidayah Cangkorah" />
        <meta property="og:image" content="{{ asset('assets/icons/ic-logo.png') }}" />
        <meta property="og:type" content="article" />
        <title>Pesantren Al-Bidayah Cangkorah</title>
    @elseif (Request::segment(1) == 'detail')
        <meta property="og:title" content="{{ $artikel->judul }}" />
        <meta name="description" content="{{ $artikel->judul }}" />
        <meta property="og:url" content="http://pesantrenalbidayahcangkorah.sch.id/detail/{{ $artikel->slug }}" />
        <meta property="og:description" content="{{ $artikel->judul }}" />
        @if ($artikel->image)
            <meta property="og:image" content="{{ asset('storage/artikel/' . $artikel->image) }}" />
        @else
            <meta property="og:image" content="{{ asset('assets/icons/ic-logo.png') }}" />
        @endif
        <meta property="og:type" content="article" />

        <title>Pesantren Al-Bidayah Cangkorah | {{ $artikel->title }}</title>
    @endif

    {{-- Meta untuk tampil di Whatsapp --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    {{-- AOS --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    {{-- magnific --}}
    <link rel="stylesheet" href="{{ asset('assets/css/magnific.css') }}">
    {{-- Summernote JS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs5.min.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs5.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
</head>

<body>
    {{-- Navbar --}}
    @include('layouts.navbar')
    {{-- content --}}
    @yield('content')
    <section id="footer" class="bg-white" data-aos="zoom-out">
        <div class="container py-4">
            <footer>
                <div class="col-12 col-md-12 mb-3">
                    <div class="row">
                        {{-- Kolom 1 Navigasi --}}
                        <div class="col-12 col-md-3 mb-3">
                            <h5 class="fw-bold mb-3">Navigasi</h5>
                            <div class="d-flex">
                                <ul class="nav flex-column me-5">
                                    <li class="nav-item mb-2"><a href="" class="nav-link p-0 text-muted">Berita
                                            Pesantren</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="" class="nav-link p-0 text-muted">Kegiatan
                                            Pesantren</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="" class="nav-link p-0 text-muted">Gallery
                                            Pesantren</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="" class="nav-link p-0 text-muted">Kegiatan
                                            Sosial</a>
                                    </li>
                                </ul>
                                <ul class="nav flex-column">
                                    <li class="nav-item mb-2"><a href="#"
                                            class="nav-link p-0 text-muted">Alumni</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Info
                                            PSB</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="/prestasi"
                                            class="nav-link p-0 text-muted">Prestasi</a>
                                    </li>
                                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Video
                                            Kegiatan</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 mb-3">
                            <h5 class="fw-bold mb-3">Follow Kami</h5>
                            <div class="d-flex mb-3">
                                <a href="" target="_blank" class="text-decoration-none text dark">
                                    <img src="{{ asset('assets/icons/ig.png') }}" height="30" width="30"
                                        class="me-4" alt="">
                                </a>
                                <a href="" target="_blank" class="text-decoration-none text dark">
                                    <img src="{{ asset('assets/icons/fb.png') }}" height="30" width="30"
                                        class="me-4" alt="">
                                </a>
                                <a href="" target="_blank" class="text-decoration-none text dark">
                                    <img src="{{ asset('assets/icons/tiktok.png') }}" height="30" width="30"
                                        class="me-4" alt="">
                                </a>
                                <a href="" target="_blank" class="text-decoration-none text dark">
                                    <img src="{{ asset('assets/icons/yt.png') }}" height="30" width="30"
                                        class="me-4" alt="">
                                </a>
                            </div>
                        </div>

                        {{-- Kolom 3 Kontak --}}
                        <div class="col-12 col-md-3 mb-3">
                            <h5 class="font-inter fw-bold mb-3">Kontak Kami</h5>
                            <ul class="nav flex-column">
                                <li class="nav-item mb-2"><a href="#"
                                        class="nav-link p-0 text-muted">albidayahcangkorah.sch.id</a>
                                </li>
                                <li class="nav-item mb-2"><a href="#"
                                        class="nav-link p-0 text-muted">@albidayahcangkorah</a>
                                </li>
                                <li class="nav-item mb-2"><a href="#"
                                        class="nav-link p-0 text-muted">0226866134</a>
                                </li>
                                <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">albidayah
                                        cangkorah</a>
                                </li>
                            </ul>
                        </div>
                        {{-- Kolom 3 Kontak --}}
                        {{-- Kolom 4 Alamat --}}
                        <div class="col-12 col-md-3 mb-3">
                            <h5 class="font-inter fw-bold mb-3">Alamat Pesantren</h5>
                            <p>Cangkorah, Kec. Batujajar, Kabupaten Bandung Barat, Jawa Barat 40561</p>
                        </div>
                        {{-- Kolom 4 Alamat --}}
                    </div>
                </div>
            </footer>
        </div>
    </section>
    <section class="bg-light border-top">
        <div class="container py-4">
            <div class="d-flex justify-content-between">
                <div>
                    Pesantren Terpadu Al-Bidayah Cangkorah
                </div>
                <div class="d-flex">
                    <p class="me-4">Syarat & Ketentuan</p>
                    <p>
                        <a href="/kebijakan" class="text-decoration-none text-dark">Kebijakan Privacy</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    {{-- jquery --}}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('assets/js/magnific.js') }}"></script>

    {{-- JQUERY --}}
    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" --}}
    {{-- crossorigin="anonymous"></script> --}}

    {{-- Summernote JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.20/summernote-bs5.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 200,
            });
        });
        const navbar = document.querySelector(".fixed-top");
        window.onscroll = () => {
            if (window.scrollY > 100) {
                navbar.classList.add("scroll-nav-active");
                navbar.classList.add("text-nav-active");

            } else {
                navbar.classList.remove("scroll-nav-active");

            }
        };

        //animasi AOS//
        AOS.init();
        //magnific
        $(document).ready(function() {
            $('.image-link').magnificPopup({
                type: 'image',
                retina: {
                    ratio: 1,
                    replaceSrc: function(item, ratio) {
                        return item.src.replace(/(\.\w{3})$/, function(m) {
                            return '@2x' + m;
                        });
                    }
                }
            });
        });
    </script>
</body>

</html>
