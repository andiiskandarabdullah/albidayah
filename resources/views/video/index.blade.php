@extends('layouts.layouts')

@section('content')
    <section id="video" class="py-5" data-aos="zoom-in">
        <div class="container py-5">
            <div class="header-berita text-center">
                <h2 class="fw-bold">Video Kegiatan Pondok Pesantren</h2>
            </div>
            <div class="row py-5" data-aos="zoom-in">
                @foreach ($video as $item)
                    <div class="col-lg-4">
                        <iframe width="100%" height="215" src="https://www.youtube.com/embed/{{ $item->youtube_code }}"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                            allowfullscreen></iframe>
                    </div>
                @endforeach
            </div>
        </div>

    </section>
@endsection
