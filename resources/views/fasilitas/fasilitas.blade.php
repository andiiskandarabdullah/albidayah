@extends('layouts.layouts')

@section('content')
    {{-- Fasilitas --}}
    <section id="fasilitas" style="margin-top: 100px" class="py-5" data-aos="zoom-in-up">
        <div class="container">
            <div class="container py-5">
                <div class="text-center">
                    <h3 class="fw-bold">Fasilitas Pesantren</h3>
                </div>
                <div class="d-flex justify-content-between align-items-center mb-5">
                </div>
                <div class="row">
                    @foreach ($facilities as $facility)
                        <div class="col-lg-3 col-md-6 col-6">
                            <a class="image-link" href="{{ asset('storage/facility/' . $facility->image) }}">
                                <img src="{{ asset('storage/facility/' . $facility->image) }}" class="img-fluid"
                                    alt="">
                            </a>
                            <p>{{ $facility->judul }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
